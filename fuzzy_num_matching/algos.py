from collections import defaultdict


def categorize(items, key, value=None, unique=False):
    def _tuple_from_keys(keys):
        return lambda x: tuple(x[f] for f in keys)

    def _parse_arg(arg):
        if callable(arg):
            return arg
        if isinstance(arg, (list, tuple)):
            return _tuple_from_keys(arg)
        if arg is not None:
            return lambda x: x[arg]
        return lambda x: x

    _key = _parse_arg(key)
    _value = _parse_arg(value)

    if unique:
        categories = defaultdict(set)
        for item in items:
            categories[_key(item)].add(_value(item))
    else:
        categories = defaultdict(list)
        for item in items:
            categories[_key(item)].append(_value(item))

    return categories


def fifo_matching(list_1, list_2, key=None, field=None, threshold=None):
    def _parse(x):
        if x is None:
            return []
        if not isinstance(x, (list, tuple)):
            return [x]
        return x

    key = _parse(key)
    field = _parse(field)
    threshold = _parse(threshold)
    assert len(field) == len(threshold), 'field and threshold should have the same length'

    n1, n2 = len(list_1), len(list_2)

    # Build up the market
    market = defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
    for i in range(n1):
        item = list_1[i]
        k = tuple(item[f] for f in key)
        for f in field:
            market[k][f][item[f]].add(i)

    # Pick off items that matched
    matched = []
    unmatched = set(range(n1))
    for i in range(n2):
        if len(unmatched) == 0:
            break
        item = list_2[i]
        k = tuple(item[f] for f in key)
        if k not in {*market}:
            continue
        choices = {*unmatched}
        for f, limit in zip(field, threshold):
            choices &= set(idx for v in {*market[k][f]} if abs(v - item[f]) <= limit for idx in market[k][f][v])
            if len(choices) == 0:
                break
        if len(choices) > 0:
            chosen = min(choices)
            matched.append((chosen, i))
            unmatched.remove(chosen)

    pairs = [(list_1[i], list_2[j]) for i, j in matched]
    remain_1 = [list_1[i] for i in sorted(unmatched)]
    remain_2 = [list_2[i] for i in sorted(set(range(n2)) - set(t[1] for t in matched))]

    return pairs, remain_1, remain_2
